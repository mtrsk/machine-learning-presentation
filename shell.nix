{ pkgs ? import ./pinned-nixpkgs.nix {} }:

let
  sysPkgs = with pkgs; [
    python37Full
    pandoc
  ];
  pythonPkgs = with pkgs.python37Packages; [
    scikitlearn
    jupyter
    numpy
    matplotlib
    tensorflowWithCuda
    tensorflow-tensorboard
    opencv3
  ];
  dev = with pkgs.python37Packages; [
    jedi
    python-language-server
    pyls-black
    pyls-isort
    pyls-mypy
  ];
  texliveEnv = pkgs.texlive.combine {
    inherit (pkgs.texlive)
      beamer
      beamertheme-metropolis
      scheme-medium
      latexmk
      pgf
      python
      textcase
      collection-basic
      collection-fontsextra
      collection-fontsrecommended
      collection-langenglish
      collection-langportuguese
      collection-latex
      collection-latexextra
      collection-mathscience
      hyphen-portuguese
      ;
  };
in
pkgs.stdenv.mkDerivation rec {
  name = "py-env-${version}";
  version = "0.0.1";
  buildInputs = sysPkgs ++ pythonPkgs ++ dev ++ [ texliveEnv ];
}
